// Copyright (C) 2010 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// Create a sparse matrix in the matrix market format.
A=sparse([1,1;1,3;2,2;3,1;3,3;3,4;4,3;4,4],..
  [9;27+%i;16;27-%i;145;88;88;121],[4,4]) ;
filename = TMPDIR+"/A.mtx";
mmwrite(filename,A);

// Read the informations from the file
mminfo(filename);
[rows,cols,entries,rep,field,symm,comm] = mminfo(filename)
A=mmread(filename);

