// Copyright (C) 2010 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

non_zeros=[1,2,3,4];
rows_cols=[1,1;2,2;3,3;4,4];
spA=sparse(rows_cols,non_zeros)
[h,rk]=lufact(sp);
b = [1 1 1 1]';
x=lusolve(h,b)
ludel(h);


