Introduction To Sparse Matrices in Scilab

Abstract
--------

The goal of this document is to present the management of 
sparse matrices in Scilab. 
We present the basic features of Scilab, which can 
create sparse matrices and can convert from and to dense  
matrices.
We show how to solve sparse linear equations in Scilab, by 
using sparse LU decomposition and iterative methods. 
We present the sparse Cholesky decomposition. 
We present the functions from the UMFPACK and TAUCS modules. 
We briefly present the internal sparse API. 
We introduce to the Arnoldi package. 
We present the Matrix Market toolbox, which reads and writes 
sparse matrix files. 
We analyze how to solve the Poisson Partial Differential 
Equation with sparse matrices. 
We present the Imsls toolbox, a set of iterative solvers 
for sparse linear systems of equations.


Authors
-------

Copyright (C) 2008-2011 - Consortium Scilab - Digiteo - Michael Baudin

Licence
-------

This document is released under the terms of the Creative Commons Attribution-ShareAlike 3.0 Unported License :
http://creativecommons.org/licenses/by-sa/3.0/

